/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fx62
 */

public class Stack {

    private Node top;

    Stack() {
    }

    boolean isEmpty() {
        return top == null;
    }

    void push(String data) {
        Node node = new Node(data);
        node.next = top;
        top = node;
    }

    String pop() {
        String value = null;
        if (isEmpty()) {
            //System.out.println("The stack is empty");
        } else {
            String aux = top.data;
            top = top.next;
            //System.out.println(aux);
            value = aux;
        }
        return value;
    }

    void list(){
        if (isEmpty()) {
            //System.out.println("The stack is empty");
        } else {
            Node temp = top;
            while (temp != null){
                //System.out.println("% " + temp.data);
                temp = temp.next;
            }
        }
    }
    
    String latest() {
        if (isEmpty()) {
            return "*";
        } else {
            return top.data;
        }
    }
}

class Node {

    Node next;
    String data;

    Node(String data) {
        this.data = data;
    }
}
