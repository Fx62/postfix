/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fx62
 */
@WebServlet(urlPatterns = {"/Postfix"})
public class Postfix extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Postfix</title>");
            out.println("</head>");
            out.println("<body>");
            //out.println("<h1>Servlet Postfix at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }*/
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        PrintWriter output = response.getWriter();
        output.println("<html><head><link href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"></head><body>");
        String original = request.getParameter("normal");
        //if (true) {
        output.println("<div class=\"post\">");
        output.println("<br><h3>La conversion postfija de la operacion original es: </h3>");
        output.println("<br>");
        String result = postfix(original);
        output.println("<b class=\"\">Infija:</b> <i>" + original + "</i><br>");
        output.println("<b>Postfija: </b><i>" + result + "</i>");
        output.println("<br><br><a href=\"index.html\">inicio</a>");
        /*} else {
            output.print("La operacion ingresada es invalida");
        }*/
        output.println("</div>");
        output.println("</html></body>");
    }

    public static String postfix(String original) {
        Stack stack = new Stack();
        //Stack reverse = new Stack();
        //Stack opt = new Stack();
        String result = "";
        String latest = "";
        byte actualPrecedence = 0;
        byte beforePrecedence = 0;
        int tempInt;
        String tempString;
        //int valueA = 0, valueB = 0, valueC = 0;
        boolean control = true;
        for (String t : fillStack(original)) {
            //System.out.print("\n" + t);
            try {
                tempInt = Integer.parseInt(t);
                result += t + " ";
                //reverse.push(t);
                //System.out.print(t);
            } catch (Exception e) {
                actualPrecedence = precedence(t);
                if (actualPrecedence != 6) {
                    //System.out.print(t + "   =>   ");
                    if (stack.isEmpty()) {
                        if (t.equals(")") || t.equals("]")) {
                            control = false;
                            break;
                        } else {
                            beforePrecedence = precedence(t);
                            stack.push(t);
                            //System.out.println("primero en la pila");
                        }
                    } else {
                        //System.out.println(actualPrecedence + "   =>   " + beforePrecedence);
                        //System.out.print(t);
                        if (actualPrecedence == 0) {
                            if (t.equals("(") || t.equals("[")) {
                                stack.push(t);
                                //System.out.println("apertura agregada");
                                beforePrecedence = actualPrecedence;
                            } else {
                                tempString = stack.pop();
                                while (!tempString.equals(null) && !tempString.equals("(") && !tempString.equals("[")) {
                                    //System.out.println(tempString + " ~");
                                    //if (!tempString.equals(")")) {
                                    result += tempString + " ";
                                    //result += t + " ";
                                    //}
                                    tempString = stack.pop();
                                    beforePrecedence = precedence(tempString);
                                }
                                tempString = stack.latest();
                                //System.out.print("^^^" + tempString);
                                //System.out.println(beforePrecedence + "limpiando cola");
                            }
                        } else {
                            if (actualPrecedence > beforePrecedence) {
                                if (beforePrecedence == 0) {
                                    //System.out.println("despues de apertura");
                                    stack.push(t);
                                    beforePrecedence = actualPrecedence;
                                } else {
                                    stack.push(t);
                                    //System.out.println("mayor prioridad");
                                    beforePrecedence = actualPrecedence;
                                }
                            } else {
                                if (actualPrecedence == beforePrecedence) {
                                    result += stack.pop() + " ";
                                    stack.push(t);
                                    //System.out.println("igual prioridad");
                                    beforePrecedence = actualPrecedence;
                                } else {
                                    result += stack.pop() + " ";
                                    //if (precedence(stack.latest()) > actualPrecedence) {
                                    //result += stack.pop() + " ";
                                    stack.push(t);
                                    //} else {
                                    
                                    beforePrecedence = actualPrecedence;
                                    //}
                                }
                            }
                        }
                    }
                } else {
                    control = false;
                    return "La operacion ingresada no es valida";
                }
            }
            //System.out.println("### " + result + "###");
            stack.list();
        }
        //System.out.println("\n" + result);
        while (!stack.isEmpty()) {
            result += stack.pop() + " ";
        }
        // System.out.println("*** " + evaluatePostfix(result) + " ***");
        result += "<br><b>Resultado: </b> " + evaluatePostfix(result);
        return result;
    }

    static String evaluatePostfix(String exp) {
        //create a stack 
        Stack stack = new Stack();
        //String tempString = "";
        double tempDouble;
        String[] temp = exp.split(" ");
        System.out.println();
        for (String t : temp) {
            System.out.print(t + "     ");
        }
        // Scan all characters one by one 
        for (int i = 0; i < temp.length; i++) {
            String c = temp[i];
            // If the scanned character is an operand (number here), 
            // push it to the stack. 
            try {
                tempDouble = Double.parseDouble(c);
                //System.out.println(tempString);
                stack.push(c);
            } //  If the scanned character is an operator, pop two 
            // elements from stack apply the operator 
            catch (Exception e) {
                double val1 = Double.parseDouble(stack.pop());
                double val2 = Double.parseDouble(stack.pop());

                switch (c) {
                    case "+":
                        stack.push(String.valueOf(val2 + val1));
                        break;

                    case "-":
                        stack.push(String.valueOf(val2 - val1));
                        break;

                    case "/":
                        System.out.println("\n\n" + val2 / val1);
                        stack.push(String.valueOf(val2 / val1));
                        break;

                    case "*":
                        stack.push(String.valueOf(val2 * val1));
                        break;
                    case "^":
                        //System.out.println("\n###\n" + Math.pow(val2, val1));
                        stack.push(String.valueOf(Math.pow(val2, val1)));
                        break;
                    default:
                        return null;
                }
            }
        }
        //System.out.print("\n\n\n" + stack.pop());
        return stack.pop();
    }

    public static byte precedence(String value) {
        byte p1 = 0;
        switch (value) {
            case "(":
                p1 = 0;
                break;
            case ")":
                p1 = 0;
                break;
            case "[":
                p1 = 0;
                break;
            case "]":
                p1 = 0;
                break;
            case "^":
                p1 = 3;
                break;
            case "*":
                p1 = 2;
                break;
            case "/":
                p1 = 2;
                break;
            case "+":
                p1 = 1;
                break;
            case "-":
                p1 = 1;
                break;
            default:
                p1 = 6;
        }
        //System.out.println("*" + p1 + "*");
        return p1;
    }

    public static String[] fillStack(String original) {
        ArrayList<String> temp = new ArrayList<String>();
        String numbers = "";
        String symbols = "";
        int tempNumber;
        String tempSymbol;
        String auxiliarString = "";
        int auxiliarInt;
        String greater = "";
        for (int i = 0; i < original.length(); i++) {
            numbers = "";
            symbols = "";
            tempSymbol = String.valueOf(original.charAt(i));
            try {
                tempNumber = Integer.parseInt(tempSymbol);
                auxiliarString += String.valueOf(tempNumber);
                try {
                    auxiliarInt = Integer.parseInt(String.valueOf(original.charAt(i + 1)));
                } catch (Exception e) {
                    temp.add(auxiliarString);
                    auxiliarString = "";
                }

            } catch (Exception e) {
                symbols += tempSymbol;
                /*
                 ****************************
                 * Here I have the operator * 
                 ****************************
                 */
                //System.out.println(symbols);
                temp.add(symbols);
            }
        }
        String[] fix = new String[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            fix[i] = temp.get(i);
            //System.out.println(fix[i]);
        }
        return fix;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
